from django.urls import path, include
from . import views

app_name = 's1'

urlpatterns = [
    path('story-1', views.story1, name='story1'),
]