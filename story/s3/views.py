from django.shortcuts import render
import datetime

# Create your views here.
def homepage(request):
    return render(request, 's3/homepage.html')

def achievements(request):
    return render(request, 's3/achievements.html')

def experience(request):
    return render(request, 's3/experience.html')

def contacts(request):
    return render(request, 's3/contacts.html')

def challenge(request):
    return render(request, 's3/Challenge3.html')

def time(request, timezone="0"):
    time = datetime.datetime.now() + datetime.timedelta(hours=int(timezone))
    return render(request, 's3/time.html', {'time':time})