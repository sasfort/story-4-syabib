from django.urls import path, include
from . import views

app_name = 's3'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('achievements', views.achievements, name='achievements'),
    path('experience', views.experience, name='experience'),
    path('contacts', views.contacts, name='contacts'),
    path('challenge', views.challenge, name='challenge'),
    path('time', views.time, name='time'),
    path('time/<str:timezone>', views.time)
]
